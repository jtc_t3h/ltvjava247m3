<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="me" uri="http://t3h.vn"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<me:bg title="Publsher">
  <jsp:attribute name="content">
<div class="page-header">Manage Author</div>
<p><a href="${pageContext.request.contextPath}/chapter4/mvc/author/add.html">Add New Author</a> </p>
<form method="post" action="${pageContext.request.contextPath}/chapter4/mvc/author/dels.html">
<table class="table"> 
  <tr>
    <th><button class="btn btn-info">Delete</button></th> 
    <th>Id</th>
    <th>Name</th>
    <th>Edit</th>
    <th>Delete</th> 
  </tr>
  
  <c:forEach items="${list}" var="obj"> 
  <tr>
    <td><input type="checkbox" value="${obj.authorId}" name="ids"></td> 
    <td>${obj.authorId}</td> 
    <td>${obj.authorName}</td> 
    <td>
      <a href="${pageContext.request.contextPath}/chapter4/mvc/author/edit.html?id=${obj.authorId}"><img src="${pageContext.request.contextPath}/images/edit.png" alt="Edit"></a> 
    </td>
    <td>
      <a href="${pageContext.request.contextPath}/chapter4/mvc/author/del.html?id=${obj.authorId}"><img src="${pageContext.request.contextPath}/images/trash.png" alt="Delete"></a> 
    </td>
  </tr> 
  </c:forEach>
</table>
</form>
</jsp:attribute>
</me:bg>