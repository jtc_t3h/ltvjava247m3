<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="me" uri="/WEB-INF/template.tld"%>
<me:bg title="Publsher">
  <jsp:attribute name="content">
    <div class="page-header">Add New Publisher</div> 
    <form method="post" class="form">
      <input type="hidden" name="id"><div>
      <label>Name</label>
      <input type="text" name="name"> </div>
      <div>
      <button class="btn btn-primary">Save</button>
      </div> 
    </form>
    
    <c:if test="${not empty msg}">
      <p class="error">${msg}</p>
    </c:if>
  </jsp:attribute>
</me:bg>