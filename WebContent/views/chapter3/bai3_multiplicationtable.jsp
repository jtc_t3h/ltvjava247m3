<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Module 03 LẬP TRÌNH WEB VỚI JAVA</title>
<link rel="stylesheet" type="text/css" href="/LTVJava247M3/css/css.css">
</head>
<body>
	<div class="title">
		<div class="container">
			<div class="brand">
				<a href="/LTVJava247M3">Module 3</a>
			</div>
		</div>
	</div>
	<div class="sidebar">
		<ul>
			<li><a href="helloworld.html">Hello World</a></li>
			<li><a href="sumtwonumbers.html">Sum Two Numbers</a></li>
			<li><a href="summultinumbers.html">Sum Multiple Numbers</a></li>
			<li><a href="register.jsp">Register</a></li>
			<li><a href="login.jsp">Login</a></li>
			<li><a href="multiplicationtable.jsp">Multiplication Table</a></li>
			<li><a href="upload.html">Upload Image</a></li>
			<li><a href="upload/multi.html">Multiple Upload Image</a></li>
			<li><a href="template.jsp">Template Example</a></li>
			<li><a href="admin/publisher.html">Publisher</a>
			<li><a href="admin/category.html">Category</a></li>
			<li><a href="home.html">Home</a></li>
			<li><a href="auth/register.html">Register</a></li>
			<li><a href="auth/logon.html">Log On</a></li>
			<li><a href="admin/invoice.html">Invoice</a></li>
		</ul>
	</div>
	<div class="main">
		<div class="page-header">Multiplication Table</div>
		<form method="post" class="form">
			<p>
				<label>Number</label> <select name="num">
					<c:forEach var="i" begin="2" end="9" step="1">
						<option value="${i}">${i}</option>
					</c:forEach>
				</select>
			</p>
			<p>
				<button>View</button>
			</p>
		</form>
		<c:if test="${pageContext.request.method == 'POST'}">
			<c:forEach var="i" begin="1" end="10" step="1">
				<p>${i}x ${param.num} = ${i * param.num}</p>
			</c:forEach>
		</c:if>
	</div>
</body>
</html>