package vn.t3h.chapter4.dao;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Random;

import vn.t3h.chapter4.domain.Member;

public class MemberDAO extends AbstractDAO {

	// Bài tập 4.7: start
	public static byte[] sha256(String plaintext) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			return digest.digest(plaintext.getBytes("UTF-8"));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
			return null;
		}
	}

	private static long random() {
		Random rand = new Random();
		long a = rand.nextInt();
		long b = rand.nextInt();
		return a * b;
	}

	public int add(Member obj) throws SQLException {
		try {
			open();
			pstmt = connection.prepareStatement(
					"INSERT INTO Member (MemberId, Username, Password, Email, Gender) VALUES(?, ?, ?, ?, ?)");
			pstmt.setLong(1, random());
			pstmt.setString(2, obj.getUsername());
//			pstmt.setBytes(3, sha256(obj.getPassword()));
			pstmt.setString(3, obj.getPassword());
			pstmt.setString(4, obj.getEmail());
			pstmt.setByte(5, obj.getGender());
			return pstmt.executeUpdate();
		} finally {
			close();
		}
	}
	// Bài tập 4.7: end

	// Bài tập 5.1: start
	public Member logOn(String username, String password) throws SQLException {
		try {
			open();
			pstmt = connection
					.prepareStatement("SELECT MemberId, Username FROM Member WHERE Username = ? AND Password = ?");
			pstmt.setString(1, username);
//			pstmt.setBytes(2, sha256(password));
			pstmt.setString(2, password);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return new Member(rs.getLong("MemberId"), rs.getString("Username"));
			}
		} finally {
			close();
		}
		return null;
	}
	// Bài tập 5.1: end
	
}
