package vn.t3h.chapter4.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import vn.t3h.chapter4.domain.Author;

public class AuthorDAO extends AbstractDAO{

	/**
	 * Lấy danh sách tác giả (Author) hiện có trong bảng Author
	 * @throws SQLException 
	 */
	public List<Author> listAll() throws SQLException {
		// đoạn code open 
		open();
		
		// Định nghĩa câu truy vấn và xử lý kết quả
		String selectSQL = "select authorid, authorname from author";
		stmt = connection.createStatement();
		rs = stmt.executeQuery(selectSQL);
		
		List<Author> list = new ArrayList<Author>();
		while(rs.next()) {
			Author author = new Author();
			author.setAuthorId(rs.getInt("authorid"));
			author.setAuthorName(rs.getString("authorname"));
			
			list.add(author);
		}
		
		// đóng kết nối
		close();
		
		return list;
	}
}
