package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.PublisherDAO;

/**
 * Servlet implementation class PublisherDelServlet
 */
@WebServlet("/chapter4/publisher/del.html")
public class Bai2_PublisherDelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_PublisherDelServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private PublisherDAO repository = new PublisherDAO();

	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getParameter("id") != null) {
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				repository.delete(id);
				response.sendRedirect(request.getContextPath() + "/chapter4/publisher/list.html");
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
