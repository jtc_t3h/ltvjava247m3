package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.ProductDAO;

/**
 * Servlet implementation class Bai3_ProductServlet
 */
@WebServlet("/chapter4/product/home.html")
public class Bai3_ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai3_ProductServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private int size = 8;
	ProductDAO repository = new ProductDAO();

	private int getPage(int total) {
		return (int) Math.ceil(total / (float) size);
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		int p = 1;
		if (req.getParameter("p") != null) {
			p = Integer.parseInt(req.getParameter("p"));
		}

		try {
			req.setAttribute("n", getPage(repository.count()));
			req.setAttribute("list", repository.getProducts(p, size));
			req.getRequestDispatcher("/views/chapter4/bai3_product_home.jsp").forward(req, res);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
