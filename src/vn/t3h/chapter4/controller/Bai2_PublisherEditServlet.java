package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.PublisherDAO;
import vn.t3h.chapter4.domain.Publisher;

/**
 * Servlet implementation class PublisherEditServlet
 */
@WebServlet("/chapter4/publisher/edit.html")
public class Bai2_PublisherEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PublisherDAO repository = new PublisherDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_PublisherEditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("o", repository.getPublisher(id));
			request.getRequestDispatcher("/views/chapter4/bai2_publisher_edit.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!request.getParameter("name").equals("")) {
			try {
				int id = Integer.parseInt(request.getParameter("id"));
				if (repository.edit(new Publisher(id, request.getParameter("name"))) > 0) {
					response.sendRedirect(request.getContextPath() + "/chapter4/publisher/list.html");
				} else {
					request.setAttribute("msg", "Edit Failed");
					doGet(request, response);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
