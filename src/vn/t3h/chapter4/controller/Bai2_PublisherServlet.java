package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.PublisherDAO;

/**
 * Servlet implementation class PublisherServlet
 */
@WebServlet("/chapter4/publisher/list.html")
public class Bai2_PublisherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PublisherDAO repository = new PublisherDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_PublisherServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			request.setAttribute("list", repository.getPublishers());
			request.getRequestDispatcher("/views/chapter4/bai2_publisher_list.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Integer> list = new LinkedList<>();
		for (String id : request.getParameterValues("ids")) {
			list.add(Integer.parseInt(id));
		}
		try {
			repository.delete(list);
			doGet(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
