package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.AuthorDAO;
import vn.t3h.chapter4.domain.Author;

/**
 * Servlet implementation class AuthorServlet
 */
@WebServlet("/chapter4/mvc/author/list.html")
public class AuthorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private AuthorDAO dao = new AuthorDAO();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			List<Author> list = dao.listAll();
			request.setAttribute("list", list);
			request.getRequestDispatcher("/views/chapter4/author_list.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
