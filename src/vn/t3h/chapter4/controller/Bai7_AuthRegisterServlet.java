package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.MemberDAO;
import vn.t3h.chapter4.domain.Member;

/**
 * Servlet implementation class Bai7_AuthRegisterServlet
 */
@WebServlet("/chapter4/auth/register.html")
public class Bai7_AuthRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai7_AuthRegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	MemberDAO repository = new MemberDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/views/chapter4/bai7_register.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			if (repository.add(new Member(0, request.getParameter("usr"), request.getParameter("pwd"),
					request.getParameter("email"), request.getParameter("fullName"),
					Byte.parseByte(request.getParameter("gender")))) > 0) {
				response.sendRedirect(request.getContextPath() + "/chapter5/auth/logon.html");
			} else {
				request.setAttribute("msg", "Register Failed");
				doGet(request, response);
			}
		} catch (NumberFormatException | SQLException e) {
			e.printStackTrace();
		}
	}

}
