package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.ProductDAO;

/**
 * Servlet implementation class Bai4_ProductDetailServlet
 */
@WebServlet("/chapter4/product/detail.html")
public class Bai4_ProductDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai4_ProductDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	ProductDAO repository = new ProductDAO();

	@Override
	public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {

		try {
			int id = Integer.parseInt(request.getParameter("id"));
			request.setAttribute("o", repository.getProduct(id));
			request.getRequestDispatcher("/views/chapter4/bai4_product_detail.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
