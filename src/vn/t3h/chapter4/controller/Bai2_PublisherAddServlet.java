package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.PublisherDAO;
import vn.t3h.chapter4.domain.Publisher;

/**
 * Servlet implementation class PublisherAddServlet
 */
@WebServlet("/chapter4/publisher/add.html")
public class Bai2_PublisherAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PublisherDAO repository = new PublisherDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai2_PublisherAddServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/views/chapter4/bai2_publisher_add.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (!request.getParameter("name").equals("")) {
			try {
				if (repository.add(new Publisher(0, request.getParameter("name"))) > 0) {
					response.sendRedirect(request.getContextPath() + "/chapter4/publisher/list.html");
				} else {
					request.setAttribute("msg", "Inserted Failed");
					doGet(request, response);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
