package vn.t3h.chapter4.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter4.dao.ProductDAO;

/**
 * Servlet implementation class Bai5_ProductSearchServlet
 */
@WebServlet("/chapter4/product/search.html")
public class Bai5_ProductSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai5_ProductSearchServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	ProductDAO repository = new ProductDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {
			request.setAttribute("list", repository.search(request.getParameter("q")));
			request.getRequestDispatcher("/views/chapter4/bai5_product_search.jsp").forward(request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
