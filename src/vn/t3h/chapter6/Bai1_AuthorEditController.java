package vn.t3h.chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter6.dao.AuthorDAO;
import vn.t3h.chapter6.domain.Author;

@WebServlet("/chapter6/author/edit.html")
public class Bai1_AuthorEditController extends HttpServlet {

	AuthorDAO repository = new AuthorDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("o", repository.getAuthor(id));
		request.getRequestDispatcher("/views/chapter6/bai1_author_edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Author obj = new Author(id, request.getParameter("name"));
		repository.edit(obj);
		response.sendRedirect(request.getContextPath() + "/chapter6/author/list.html");
	}
}
