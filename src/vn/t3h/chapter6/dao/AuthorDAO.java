package vn.t3h.chapter6.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import vn.t3h.chapter6.domain.Author;

public class AuthorDAO {

	public List<Author> search(String q) {
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			final Query query = session.createQuery("From Author WHERE AuthorName LIKE :q");
			query.setParameter("q", '%' + q + '%');

			return query.list();
		}
	}

	public boolean delete(int id) {
		Transaction tran = null;
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			tran = session.beginTransaction();
			session.delete(new Author(id, null));
			tran.commit();
			return true;
		} catch (Exception ex) {
			if (tran != null) {
				tran.rollback();
			}
			return false;
		}
	}

	public List<Author> getAuthors() {
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			System.out.println("aaaaaaaaaaaaa");
			return session.createQuery("From Author").list();
		}
	}

	public Author getAuthor(int id) {
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			return session.get(Author.class, id);
		}
	}

	public boolean edit(Author obj) {
		Transaction tran = null;
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			tran = session.beginTransaction();
			session.update(obj);
			tran.commit();
			return true;
		} catch (Exception e) {
			if (tran != null) {
				tran.rollback();
			}
			return false;
		}
	}

	public boolean add(Author obj) {
		Transaction tran = null;
		try (Session session = HibernateUtil.getSessionfactory().openSession()) {
			tran = session.beginTransaction();
			session.save(obj);
			tran.commit();
			return true;
		} catch (Exception e) {
			if (tran != null) {
				tran.rollback();
			}
			return false;
		}
	}
	
	public int delete(int[] ids) {
		
		try {
			// SessionFactory
			SessionFactory sessionFactory = HibernateUtil.getSessionfactory();
			
			// Session
			Session session = sessionFactory.openSession();
			
			// thuc hien tac vu chinh -> delete
			int ret = 0;
			for (int id: ids) {
				Author author = session.get(Author.class, id);
				session.delete(author);
				ret += 1;
			}
			return ret;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return 0;
	}
}
