package vn.t3h.chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter6.dao.AuthorDAO;
import vn.t3h.chapter6.domain.Author;

/**
 * Servlet implementation class Bai1_AuthorAddController
 */
@WebServlet("/chapter6/author/add.html")
public class Bai1_AuthorAddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_AuthorAddController() {
		super();
		// TODO Auto-generated constructor stub
	}

	AuthorDAO repository = new AuthorDAO();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/views/chapter6/bai1_author_add.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Author obj = new Author(null, request.getParameter("name"));
		repository.add(obj);
		response.sendRedirect(request.getContextPath() + "/chapter6/author/list.html");
	}
}
