package vn.t3h.chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter6.dao.AuthorDAO;

/**
 * Servlet implementation class Bai1_AuthorController
 */
@WebServlet("/chapter6/author/list.html")
public class Bai1_AuthorController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Bai1_AuthorController() {
        super();
        // TODO Auto-generated constructor stub
    }

    AuthorDAO repository = new AuthorDAO();
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.setAttribute("list", repository.getAuthors()); 
    	request.getRequestDispatcher("/views/chapter6/bai1_author_list.jsp").forward(request,response); 
    }
    
}
