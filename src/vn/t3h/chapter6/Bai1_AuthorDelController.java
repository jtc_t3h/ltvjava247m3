package vn.t3h.chapter6;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.t3h.chapter6.dao.AuthorDAO;

@WebServlet("/chapter6/author/del.html")
public class Bai1_AuthorDelController extends HttpServlet {
	
	private static final long serialVersionUID = 1913955143730064705L;
	
	AuthorDAO repository = new AuthorDAO();
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));
		repository.delete(id);
		resp.sendRedirect(req.getContextPath() + "/chapter6/author/list.html");
	}
}
