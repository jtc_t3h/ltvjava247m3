package vn.t3h.chapter5;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter4.dao.MemberDAO;
import vn.t3h.chapter4.domain.Member;

/**
 * Servlet implementation class Bai1_AuthLogOnController
 */
@WebServlet("/chapter5/auth/logon.html")
public class Bai1_AuthLogOnController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	MemberDAO dao = new MemberDAO();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_AuthLogOnController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/views/chapter5/bai1_logon.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Kiểm tra đăng nhập thành công hay là không?
		try {
			Member ret = dao.logOn(request.getParameter("usr"), request.getParameter("pwd"));

			if (ret != null) {
				// login successful
				savedSession(ret, request, response);
				response.sendRedirect(request.getContextPath() + "/chapter5/home.html");
			} else {
				request.setAttribute("msg", "Log On Failed");
				doGet(request, response);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void savedCookie(HttpServletRequest request, HttpServletResponse response) {
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equals("JSESSIONID")) {
				cookie.setMaxAge(30 * 24 * 3600);
				cookie.setPath(request.getContextPath());
				response.addCookie(cookie);
			}
		}
	}

	private static void savedSession(Member obj, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.setAttribute("member", obj);  // Tạo mới 1 thuộc tính -> Container sẽ gọi hàm attributeAdded của bộ lắng nghe.
		if (request.getParameter("remember") != null) {
			savedCookie(request, response);
		}
	}

}
