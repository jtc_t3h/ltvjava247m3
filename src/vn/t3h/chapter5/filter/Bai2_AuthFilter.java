package vn.t3h.chapter5.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class Bai2_AuthFilter
 */
@WebFilter("/chapter5/*")
public class Bai2_AuthFilter implements Filter {

    /**
     * Default constructor. 
     */
    public Bai2_AuthFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		// Nếu chưa tồn tại Session -> Tạo mới Session
		// Đã tồn tại rồi -> trả vê session hiện tại
		HttpSession session = req.getSession(); // Gọi Bộ lắng nghe xử kiên createSession
		
		// Kiểm tra đã đăng nhập hay chưa? Đã nhập thì cho đi, ngược lại chuyển sang trang đăng 
		if (session.getAttribute("member") == null && !req.getRequestURI().endsWith("logon.html")) {
			res.sendRedirect("auth/logon.html");
		} else {

			// pass the request along the filter chain
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
