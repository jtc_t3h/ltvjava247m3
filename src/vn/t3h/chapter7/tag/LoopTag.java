package vn.t3h.chapter7.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.IterationTag;
import javax.servlet.jsp.tagext.Tag;

public class LoopTag implements IterationTag{

	private Tag parent;
	private PageContext pc;
	private String var;
	private int begin;
	private int end;
	
	
	public void setVar(String var) {
		this.var = var;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	@Override
	public int doEndTag() throws JspException {
		// TODO Auto-generated method stub
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		// do something
		pc.setAttribute(var, String.valueOf(begin));
		return EVAL_BODY_INCLUDE;
	}

	@Override
	public Tag getParent() {
		return parent;
	}

	@Override
	public void release() {
		// TODO Auto-generated method stub
	}

	@Override
	public void setPageContext(PageContext pc) {
		this.pc = pc;
	}

	@Override
	public void setParent(Tag parent) {
		this.parent = parent;
	}

	@Override
	public int doAfterBody() throws JspException {
		// sử dụng EL cho custom Tag
		
		if (++begin > end) {
			return SKIP_BODY;
		}
		pc.setAttribute(var, String.valueOf(begin));
		return EVAL_BODY_AGAIN;
	}

}
