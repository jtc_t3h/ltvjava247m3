package vn.t3h.chapter7.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

public class GreetTag implements Tag {

	private String name;
	private Tag parent;
	private PageContext pageContext;

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}

	@Override
	public int doStartTag() throws JspException {
		// TO DO
		JspWriter out = pageContext.getOut();
		try {
			out.print("Hello " + name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return SKIP_BODY;
	}

	@Override
	public Tag getParent() {
		return parent;
	}

	@Override
	public void release() {
	}

	@Override
	public void setPageContext(PageContext pc) {
		this.pageContext = pc;
	}

	@Override
	public void setParent(Tag parent) {
		this.parent = parent;
	}

	public void setName(String name) {
		this.name = name;
	}

}
