package vn.t3h.chapter8;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter4.dao.ProductDAO;
import vn.t3h.chapter4.domain.Product;
import vn.t3h.chapter5.domain.Cart;

/**
 * Servlet implementation class Bai3_CartController
 */
@WebServlet("/chapter8/cart.html")
public class Bai1_CartController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_CartController() {
		super();
		// TODO Auto-generated constructor stub
	}

	ProductDAO repository = new ProductDAO();

	private static Map<Integer, Cart> getCarts(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Object obj = session.getAttribute("cart");
		Map<Integer, Cart> carts = null;
		if (obj != null) {
			carts = (Map<Integer,Cart>) obj;
		} else {
			carts = new HashMap<Integer, Cart>();
			session.setAttribute("cart", carts);
		}
		return carts;
	}

	private void saveCarts(Map<Integer, Cart> carts, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.setAttribute("cart", carts);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("list", getCarts(request, response).values());
		request.getRequestDispatcher("/views/chapter8/bai1_cart_index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			Map<Integer, Cart> carts = getCarts(request, response);
			int id = Integer.parseInt(request.getParameter("id"));
			short qty = Short.parseShort(request.getParameter("qty"));
			if (carts.containsKey(id)) {
				carts.get(id).increaseQuantity(qty);
			} else {
				Product obj = repository.getProduct(id);
				Cart cart = new Cart(obj.getId(), obj.getPrice(), qty, obj.getImageUrl(), obj.getTitle());
				carts.put(id, cart);
			}
			saveCarts(carts, request, response);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		doGet(request, response);
	}
}
