package vn.t3h.chapter8;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import vn.t3h.chapter5.domain.Cart;

/**
 * Servlet implementation class Bai1_CartEditController
 */
@WebServlet("/chapter8/cart/edit.html")
public class Bai1_CartEditController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Bai1_CartEditController() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static Map<Integer, Cart> getCarts(HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		Object obj = session.getAttribute("cart");
		Map<Integer, Cart> carts = null;
		if (obj != null) {
			carts = (Map<Integer, Cart>) obj;
		} else {
			carts = new HashMap<Integer, Cart>();
			session.setAttribute("cart", carts);
		}
		return carts;
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		try (PrintWriter pw = response.getWriter()) {
			short qty = Short.parseShort(request.getParameter("qty"));
			int key = Integer.parseInt(request.getParameter("id"));
			Map<Integer, Cart> carts = getCarts(request, response);
			if (carts.containsKey(key)) {
				carts.get(key).setQuantity(qty);
			}
			pw.write("1");
		}
	}
}
