package vn.t3h.chapter2;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/chapter2/LoginSrv")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../views/chapter2/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Lấy dữ liệu người dùng nhập
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		// kiểm tra username và password có hợp lệ (phải nhập) -> username = password hoặc thông qua đối số (ServletContext) 
		if (!username.isEmpty() && !password.isEmpty() && username.equals(password)) {
			
			
			HttpSession session = request.getSession();
			session.setAttribute("username", username);
			response.sendRedirect("../views/chapter2/welcome.jsp");
		} else {
			request.setAttribute("msgError", "Username or Passord incorrect.");
			request.getRequestDispatcher("../views/chapter2/login.jsp").forward(request, response);
		}
		
	}

}
