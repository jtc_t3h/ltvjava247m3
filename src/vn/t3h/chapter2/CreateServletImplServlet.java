package vn.t3h.chapter2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CreateServletImplServlet implements Servlet {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		System.out.println("CreateServletImplServlet: destroy method.");
	}

	@Override
	public ServletConfig getServletConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getServletInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		System.out.println("CreateServletImplServlet: init method");
		
		// TH: neu biet duoc param-name -> goi ham getParameterName
		System.out.println(config.getInitParameter("adminEmail"));
		System.out.println(config.getInitParameter("adminNumber"));
		
		// TH: duyet het
		Enumeration<String> paramNames = config.getInitParameterNames();
		while(paramNames.hasMoreElements()) {
			String paraName = paramNames.nextElement();
			System.out.println(paraName + " = " + config.getInitParameter(paraName));
		}
		
		// ServletContext
		ServletContext context = config.getServletContext();
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = res.getWriter();
		out.print("<html><body><h1>Create Servlet by Implement Servlet Interface.</h1></body></html>");
		
		ServletConfig config = getServletConfig();
		ServletContext context = getServletConfig().getServletContext();
	}

}
