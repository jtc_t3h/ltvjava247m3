package vn.t3h.chapter2;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class CreateServletExtGenericServlet extends GenericServlet {

	
	
	@Override
	public void init() throws ServletException {
		System.out.println("CreateServletExtGenericServlet: init method.");
	}

	@Override
	public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
		PrintWriter out = res.getWriter();
		out.print("<html><body><h1>Create Servlet by Extends GenericServlet Class.</h1></body></html>");
		
	}

}
