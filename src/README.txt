------------------ Chương 2: Servlet -------------------------------------------
- Mục tiêu
  + Tạo Servlet
  + Chu trình sống của một Servlet
  + Servlet Config
  + Servlet Context
  + Chuyển trang (servlet <-> Servlet, JSP <-> JSP và Servlet <-> JSP)
  + Cấu trúc ưng dụng đã được triển khai trong Web Server (apache Tomcat) (optional)

- Tạo Servlet (2 bước)
  + B1: Tạo 1 class -> có 3 cách
	+ Hiện thực giao diện Servlet
	+ Hoặc kế thừa lớp GenericServlet
	+ Hoặc kế thừa lớp HttpServlet (Lớp HttpServlet kế thừa lớp GenericServlet)
  + B2: Cấu hình -> 2 cách
    - Định nghĩa thẻ Servlet và Servlet-Mapping trong web.xml
	- Dùng Annotation @WebServlet
	
	
- Chu trình sống của Servlet
  + Đối tượng của một Servlet được tạo ra khi nào? -> 2 thời điểm
    - Khi có request từ client
	- Được khởi trước khi ứng dụng được deploy (khai báo load-on-startup)
  + Life-cycle callback method:
	- init() -> được goi sau khi Container tạo đối tượng Servlet
	- service() -> được gọi khi có request từ client
	- destroy() -> được gọi trước khi undeploy ứng dụng
  
- Servlet config: định nghĩa một số thông số (cấu hình) cho một Servlet
 + Cấu hình
   - Dùng thẻ init-param (trong cấu hình web.xml)
   - Dùng thuộc tính initParams trong @WebServlet
 + Dùng đối tượng ServetConfig để lưu trữ thông tin từ Init-param (Trong servlet)
   - Để truy xuất một param-name -> getInitParameter()
   - Để truy xuất tất cả các thông số -> duyệt tất cả param-name: getInitParameterNames()
   - Sử dụng ServletConfig trong hàm service, doPost, doGet -> dùng getServletConfig()
   
- Servlet context: tương tự như ServletConfig nhưng được truy xuất bởi tất cả Servlet
 + Cấu hình dùng thẻ context-param trong web.xml
 + Trong Servlet có thể truy Servlet context:
   - Dùng hàm getServletContext()
   - Dùng thông qua đối tượng ServletConfig -> tạo đối tượng ServletContext
   
- Chuyển trang (servlet <-> Servlet, JSP <-> JSP và Servlet <-> JSP): 2 cách
 + Dùng đối tượng Request -> gọi hàm getRequestDispatcher -> và gọi forward()
   - URL không thay đổi
   - Cách xác định URL trong Servlet (dùng .. để lùi 1 thư mục)
 + Dung đối tượng Response -> gọi hàm sendRedirect() 
 
??? So sánh sự khác biệt giữa 2 phương pháp.
 
 -------------------- Chương 4: MVC ------------------------------------------------------
 1. Tầng Model 
 + domain data (Hibernate) = Entity (EJB)  (với Hibernate, EJB gọi ORM - object relation mapping) => Java Bean 
 + bussiness logic => thao tác xuống bảng dữ liệu (CRUD) = Data Access Layer = DAL => lớp cụ thể DAO - Data Access Object
 + mapping => ORM => ánh xạ một Entity <-> bảng trong CSDL
 
 2. Tầng Controller:
 + Sử dụng Servlet -> hiện thực 2 phương thức doGet và doPost (không hiện thưc hàm service)
 
 3. Tầng View:
 + JSP (HTML + Java Code)
 + EL = Expression Language
 + JSTL
 
 -------------------- Chương 5: Session, Cookie, Filter, Event - Listener ----------------
 1. Session, Cookie
 - Web sử dụng giao thức HTTP(S) -> stateless - không có trạng -> Server không xác được các lần gọi khác nhau là của 1 client hay nhiều client
 -> Nhu cầu của web bán hàng thì cần xác định trạng 
 -> Hỗ trợ kỹ thuật Session, Cookie
 
 a. Session 
 - Tất cả dữ liệu lưu bên phía server -> Tốn tài nguyên lưu 
 - Giữa client - server dùng session id (trong trình duyệt JSESSIONID)
 b. Cookie
 - Giữa client - server: tất cả dữ liệu sẽ được trao đổi qua lại trong quá request và response
 -> Tốn băng thông mạng
 
 c. check session, cookie trên một số trình duyệt:
 - Chrome: Setting -> expand advance -> Site setting -> Cookies -> See all Cookie and data -> Tìm kiếm site cần kiểm tra
 - Safari: preference -> privacy -> manage website data
  
 2. Filter
 - chặn request trước khi đến servlet (controller)
 - Tạo filter -> có 2 bước:
   + Bước 1: Tạo 1 lớp implement Filter -> hiện thực các hàm lifecycle callback 
     - init
     - doFilter
     - destroy
   + Cấu hình web.xml: có 2 thẻ:
     - filter
     - filter-mapping
   + Hoặc cấu hình dùng Annotation
     
3. Event - Listener
- Event: Trong Web sẽ qui định event -> danh sách giao diện Listerner
  + khởi tạo hoặc huỷ servlet context
  + Tạo mới session 
  + Tạo mới một thuộc tính của session
  + ...
- Tạo bộ lắng nghe -> Tạo lớp hiện thực listener
  + Bước 1: Tạo lớp implement tương ứng với sự kiện
  + Bước 2: Cấu hình (Web.xml hoặc Annotation)
 
 ---------------- Chapter 6: Hibernate -------------------------------------------------------------
 Theo mô hình MVC -> Hibernate hỗ trợ tầng bussiness login (tầng nghiệp vụ) -> model:
 - Domain data = Entity: Ánh xạ xuống 1 bảng trong CSDL
   + Quan hệ giữa các Entity: one-to-many, many-to-many, one-to-one
   + Quan hệ thừa kế, đa hình
 - Data access layer - DAL: chứa nhiều DAO -> thực hiện các tác vụ CRUD -> cụ thể:
   + Tạo ra đối tượng SessionFactory <- Từ Datasource (CSDL)
   + Tạo đối tượng Session: thành phần trung tâm:
     - Tác vụ cơ ban: gọi API của Session -> save, update (EJB gọi merger), find, remove
     - Tác vụ phức tạp: -> thông qua đối tượng Query thực thi câu truy vấn -> 2 dạng câu truy vấn
       + HQL (JPQL)
       + SQL (Native query)
=> Hiện thực thực tế -> Opened-closed princicple
  - Tạo giao diện (interface)
  - Class hiện giao diện
       
=> các tầng khác (controller và view) tương tự các chương khác.
     
 ------------------------------- Chapter7: Customer Tag ---------------------------------------------------
 1. Hạn chế của JSP
 - Trong trang JSP thực tế -> bỏ Java code
 - JSP chỉ hỗ trợ một số thẻ (tag) -> JSP action:
   + jsp:include
   + jsp:forward
   + jsp:userbean
   + jsp:setProperty
   + jsp:getProperty

2. Custom Tag -> kỹ thuật tạo ra JSTL (c:if, c:foreach,...) => Hiện thực custom Tag:
- TLD (tag library descriptor): mô tả cú pháp của custom tag cần hiện thực
  + DTD Taglib
    - x?: optional
    - x*: thẻ x xuất hiện [0-n]
    - x+: thẻ x xuất hiện [1-n]
    - x: required
  + DTD Tag
    - tag-class: Tag Handler -> là lớp Java để xử lý cho tag
    - body-content: có giá trị thuộc 1 trong 3 dạng
      + empty
      + JSP
      + Tagdependency
    - attribute
   -> Tạo file TLD dựa trên cấu trúc XML -> sử dụng Catalog Entry
   -> chọn web-jspgtaglib_2_1.xml
- Tag Handler -> một hiện thực 1 trong cách sau:
  + Hiện thực 1 trong 3 giao diện:
    - Tag
    - IterationTag
    - Body
  + Thừa kế lớp Tag Support
- Sử dụng custom tag
  + Khai báo 
  + sử dụng 
 










 
 
 
 
 
 
 
 
 
 
 